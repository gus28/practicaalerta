package com.example.practicaalerta.ViewModels;

import com.example.practicaalerta.clases.VistaU;

import java.util.ArrayList;

public class Vista_Usuario {

    public String estado;
    public ArrayList<VistaU>visualizaciones;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public ArrayList<VistaU> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(ArrayList<VistaU> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }
}
