package com.example.practicaalerta.ViewModels;

import com.example.practicaalerta.clases.AlertasUs;

import java.util.ArrayList;

public class Alertas {

    public ArrayList<AlertasUs>alertas;

    public ArrayList<AlertasUs> getAlertas() {
        return alertas;
    }

    public void setAlertas(ArrayList<AlertasUs> alertas) {
        this.alertas = alertas;
    }
}
