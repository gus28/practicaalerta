package com.example.practicaalerta.ViewModels;

import com.example.practicaalerta.clases.AlertasU;

//import java.util.ArrayList;
import java.util.List;

public class Alertas_Usuario {

    public String estado;
    public List<AlertasU> alertas;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }


    public List<AlertasU> getAlertas() {
        return alertas;
    }

    public void setAlertas(List<AlertasU> alertas) {
        this.alertas = alertas;
    }
}
