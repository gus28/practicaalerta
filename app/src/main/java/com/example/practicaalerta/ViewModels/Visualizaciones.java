package com.example.practicaalerta.ViewModels;

import com.example.practicaalerta.clases.VistaGeneral;

import java.util.ArrayList;

public class Visualizaciones {

    public ArrayList<VistaGeneral>visualizaciones;

    public ArrayList<VistaGeneral> getVisualizaciones() {
        return visualizaciones;
    }

    public void setVisualizaciones(ArrayList<VistaGeneral> visualizaciones) {
        this.visualizaciones = visualizaciones;
    }
}
