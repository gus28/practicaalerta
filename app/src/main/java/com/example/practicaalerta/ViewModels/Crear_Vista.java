package com.example.practicaalerta.ViewModels;

public class Crear_Vista {
    public String estado;
    public String detalle;
    public String usuarioId;
    public String alertaId;

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }

    public String getUsuarioId() {
        return usuarioId;
    }

    public void setUsuarioId(String usuarioId) {
        this.usuarioId = usuarioId;
    }

    public String getAlertaId() {
        return alertaId;
    }

    public void setAlertaId(String alertaId) {
        this.alertaId = alertaId;
    }
}
