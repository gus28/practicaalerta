package com.example.practicaalerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practicaalerta.ViewModels.Alertas;
import com.example.practicaalerta.ViewModels.Alertas_Usuario;
import com.example.practicaalerta.ViewModels.Crear_Alerta;
import com.example.practicaalerta.api.Api;
import com.example.practicaalerta.api.Servicios.ServicioPeticion;
import com.example.practicaalerta.clases.AlertasU;
import com.example.practicaalerta.clases.AlertasUs;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Principal extends AppCompatActivity {

    public TextView IdUsuarios;
    public Button Talertas, Ualertas, CrearAlerta, Vista;

    public ListView listAlertas;

    public ArrayList C_alertas = new ArrayList();
    public ArrayList C_alertasU = new ArrayList();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_principal);

        getSupportActionBar().hide();
        IdUsuarios = (TextView)findViewById(R.id.tv_idUser);
        SharedPreferences preferenciasId = getSharedPreferences("credencialesId", Context.MODE_PRIVATE);
        String name = preferenciasId.getString("NAME","");
        if(name != ""){
            IdUsuarios.setText(name);
        }


        listAlertas = (ListView)findViewById(R.id.list);
        Talertas = (Button) findViewById(R.id.btn_alertasG);
        Ualertas = (Button)findViewById(R.id.btn_alertasU);
        CrearAlerta = (Button)findViewById(R.id.btn_crearAlerta);
        Vista = (Button)findViewById(R.id.btn_vistas);

        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,C_alertas);
        final ArrayAdapter arrayAdapter2 = new ArrayAdapter(this,android.R.layout.simple_list_item_1,C_alertasU);
        
        Talertas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ServicioPeticion service = Api.getApi(Principal.this).create(ServicioPeticion.class);
                Call<Alertas> AlertasCall = service.getAlertas();
                AlertasCall.enqueue(new Callback<Alertas>() {
                    @Override
                    public void onResponse(Call<Alertas> call, Response<Alertas> response) {
                        Alertas peticion = response.body();



                        List<AlertasUs> Pe_Alerats = peticion.alertas;

                        for (AlertasUs mostrar : Pe_Alerats) {
                            C_alertas.add(mostrar.getUsuarioId());
                        }
                        listAlertas.setAdapter(arrayAdapter);
                        arrayAdapter2.clear();


                    }

                    @Override
                    public void onFailure(Call<Alertas> call, Throwable t) {

                        Toast.makeText(Principal.this, "Error", Toast.LENGTH_LONG).show();

                    }
                });
                
            }
        });



        Ualertas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ServicioPeticion service = Api.getApi(Principal.this).create(ServicioPeticion.class);
                Call<Alertas_Usuario> AlertasUCall = service.getAlertasUsuario(IdUsuarios.getText().toString());
                AlertasUCall.enqueue(new Callback<Alertas_Usuario>() {
                    @Override
                    public void onResponse(Call<Alertas_Usuario> call, Response<Alertas_Usuario> response) {
                        Alertas_Usuario peticion = response.body();

                        if (peticion.estado == "true"){

                            List<AlertasU> Pe_Alerats = peticion.alertas;

                            for (AlertasU mostrar : Pe_Alerats) {
                                C_alertasU.add(mostrar.getUsuarioId());
                            }
                            listAlertas.setAdapter(arrayAdapter2);
                            arrayAdapter.clear();

                        }else {

                            Toast.makeText(Principal.this, "No se encuentra ese dato", Toast.LENGTH_SHORT).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<Alertas_Usuario> call, Throwable t) {

                        Toast.makeText(Principal.this, "Error", Toast.LENGTH_LONG).show();

                    }
                });

            }
        });

        CrearAlerta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ServicioPeticion service = Api.getApi(Principal.this).create(ServicioPeticion.class);
                Call<Crear_Alerta> CrearAlertaCall = service.getCrearAlerta(IdUsuarios.getText().toString());
                CrearAlertaCall.enqueue(new Callback<Crear_Alerta>() {
                    @Override
                    public void onResponse(Call<Crear_Alerta> call, Response<Crear_Alerta> response) {
                        Crear_Alerta peticion = response.body();

                        if (response.body() == null){
                            Toast.makeText(Principal.this,"Ocurrio un error, intentalo mas tarde", Toast.LENGTH_LONG).show();
                            return;
                        }
                        if(peticion.estado == "true"){

                            Toast.makeText(Principal.this, "Alerta registrada", Toast.LENGTH_LONG).show();
                        }else{
                            Toast.makeText(Principal.this, "No se registro la alerta", Toast.LENGTH_LONG).show();
                        }

                    }

                    @Override
                    public void onFailure(Call<Crear_Alerta> call, Throwable t) {

                        Toast.makeText(Principal.this, "Error", Toast.LENGTH_LONG).show();

                    }
                });

            }
        });

        Vista.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Principal.this, Visualizacion.class));
            }
        });




    }


}