package com.example.practicaalerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practicaalerta.ViewModels.Peticion_Login;
import com.example.practicaalerta.api.Api;
import com.example.practicaalerta.api.Servicios.ServicioPeticion;

import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    private Button iniciar;

    private TextView crear;
    private String APITOKEN = "";
    private String IDUSUARIO = "";

    private EditText correoet;
    private EditText passwordet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        getSupportActionBar().hide();

        Verificar();

        //__________________________________________________________________

        iniciar = (Button) findViewById(R.id.btn_Iniciar);
        crear = (TextView) findViewById(R.id.tv_crear);

        iniciar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                correoet = (EditText)findViewById(R.id.et_Icorreo);
                passwordet = (EditText) findViewById(R.id.et_Ipass);


                if (correoet.getText().toString().isEmpty() || correoet.getText().toString() == ""){
                    correoet.setSelectAllOnFocus(true);
                    correoet.requestFocus();
                    correoet.setError("Inserta un correo");
                    return;
                }

                if (passwordet.getText().toString().isEmpty() || passwordet.getText().toString() == ""){
                    passwordet.setSelectAllOnFocus(true);
                    passwordet.requestFocus();
                    passwordet.setError("Inserta una contraseña");
                    return;
                }

                realizarlogin();

            }
        });

        crear.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent1 = new Intent(MainActivity.this, Registro.class);
                startActivity(intent1);
            }
        });

        //______________________________________________________________________________________
    }

    public void realizarlogin(){

        ServicioPeticion service = Api.getApi(MainActivity.this).create(ServicioPeticion.class);
        retrofit2.Call<Peticion_Login> loginCall = service.getLogin(correoet.getText().toString(),passwordet.getText().toString());
        loginCall.enqueue(new Callback<Peticion_Login>() {
            @Override
            public void onResponse(retrofit2.Call<Peticion_Login> call, Response<Peticion_Login> response) {
                Peticion_Login peticion = response.body();
                if(peticion.estado == "true"){

                    APITOKEN = peticion.token;
                    guardarPreferencias();
                    IDUSUARIO = peticion.id;
                    guardarId();

                    Toast.makeText(MainActivity.this, "Bienvenido", Toast.LENGTH_LONG).show();


                    startActivity(new Intent(MainActivity.this, Principal.class));
                }else {
                    Toast.makeText(MainActivity.this, "Datos incorrectos", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(retrofit2.Call<Peticion_Login> call, Throwable t) {
                Toast.makeText(MainActivity.this,"Error",Toast.LENGTH_LONG).show();

            }
        });

    }

    //__________________________________________________________________________________________________

    public void guardarPreferencias (){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = APITOKEN;
        SharedPreferences.Editor editor = preferencias.edit();
        editor.putString("TOKEN", token);
        editor.commit();
    }

    public void guardarId(){
        SharedPreferences preferenciasId = getSharedPreferences("credencialesId", Context.MODE_PRIVATE);
        String name = IDUSUARIO;
        SharedPreferences.Editor editor = preferenciasId.edit();
        editor.putString("NAME", name);
        editor.commit();

    }

    //_____________________________________________________________________________________________________

    public void Verificar(){
        SharedPreferences preferencias = getSharedPreferences("credenciales", Context.MODE_PRIVATE);
        String token = preferencias.getString("TOKEN","");
        if(token != ""){
            Toast.makeText(MainActivity.this,"Bienvenido Nuevamente", Toast.LENGTH_LONG).show();
            startActivity(new Intent(MainActivity.this,Principal.class));
        }

    }

    //_______________________________________________________________________________________________________
}