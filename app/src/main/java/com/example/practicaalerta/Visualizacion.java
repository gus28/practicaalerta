package com.example.practicaalerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;


import com.example.practicaalerta.ViewModels.Alertas_Usuario;
import com.example.practicaalerta.ViewModels.Vista_Usuario;
import com.example.practicaalerta.ViewModels.Visualizaciones;
import com.example.practicaalerta.api.Api;
import com.example.practicaalerta.api.Servicios.ServicioPeticion;
import com.example.practicaalerta.clases.AlertasU;
import com.example.practicaalerta.clases.VistaGeneral;
import com.example.practicaalerta.clases.VistaU;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Visualizacion extends AppCompatActivity {

    public TextView idusuario;
    public Button VistaG, VistaUsuario;

    public ListView listVistas;
    public ArrayList C_vistas = new ArrayList();
    public ArrayList C_vistasU = new ArrayList();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_visualizacion);

        getSupportActionBar().hide();
        idusuario = (TextView)findViewById(R.id.tv_iduser);
        SharedPreferences preferenciasId = getSharedPreferences("credencialesId", Context.MODE_PRIVATE);
        String name = preferenciasId.getString("NAME","");
        if(name != ""){
            idusuario.setText(name);
        }

        VistaG = (Button)findViewById(R.id.btn_vistaG);
        VistaUsuario = (Button)findViewById(R.id.btn_vistaU);
        listVistas = (ListView)findViewById(R.id.list2);

        final ArrayAdapter arrayAdapter = new ArrayAdapter(this,android.R.layout.simple_list_item_1,C_vistas);
        final ArrayAdapter arrayAdapter2 = new ArrayAdapter(this,android.R.layout.simple_list_item_1,C_vistasU);

        VistaG.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ServicioPeticion service = Api.getApi(Visualizacion.this).create(ServicioPeticion.class);
                Call<Visualizaciones> VistasCall = service.getVistas();
                VistasCall.enqueue(new Callback<Visualizaciones>() {
                    @Override
                    public void onResponse(Call<Visualizaciones> call, Response<Visualizaciones> response) {
                        Visualizaciones peticion = response.body();



                        List<VistaGeneral> Pe_vistas = peticion.visualizaciones;

                        for (VistaGeneral mostrar : Pe_vistas) {
                            C_vistas.add(mostrar.getUsuarioId());
                        }
                        listVistas.setAdapter(arrayAdapter);
                        arrayAdapter2.clear();


                    }

                    @Override
                    public void onFailure(Call<Visualizaciones> call, Throwable t) {

                        Toast.makeText(Visualizacion.this, "Error", Toast.LENGTH_LONG).show();

                    }
                });

            }
        });

        VistaUsuario.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ServicioPeticion service = Api.getApi(Visualizacion.this).create(ServicioPeticion.class);
                Call<Vista_Usuario>VistaUCall = service.getVistaUsuario(idusuario.getText().toString());
                VistaUCall.enqueue(new Callback<Vista_Usuario>() {
                    @Override
                    public void onResponse(Call<Vista_Usuario> call, Response<Vista_Usuario> response) {
                        Vista_Usuario peticion = response.body();

                        if (peticion.estado == "true"){

                            List<VistaU> Pe_vistasU = peticion.visualizaciones;

                            for (VistaU mostrar : Pe_vistasU) {
                                C_vistasU.add(mostrar.getUsuarioId());
                            }
                            listVistas.setAdapter(arrayAdapter2);
                            arrayAdapter.clear();

                        }else {

                            Toast.makeText(Visualizacion.this, "No se encuentra ese dato", Toast.LENGTH_SHORT).show();

                        }

                    }

                    @Override
                    public void onFailure(Call<Vista_Usuario> call, Throwable t) {

                        Toast.makeText(Visualizacion.this, "Error", Toast.LENGTH_LONG).show();

                    }
                });

            }
        });
    }
}