package com.example.practicaalerta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.practicaalerta.ViewModels.Crear_Alerta;
import com.example.practicaalerta.ViewModels.Crear_Vista;
import com.example.practicaalerta.api.Api;
import com.example.practicaalerta.api.Servicios.ServicioPeticion;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CrearV extends AppCompatActivity {

    public TextView usuarioCiD, alertaD;

    public Button atras;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_crear_v);

        getSupportActionBar().hide();
        usuarioCiD = (TextView)findViewById(R.id.tv_idUsuarioC);
        SharedPreferences preferenciasId = getSharedPreferences("credencialesId", Context.MODE_PRIVATE);
        String name = preferenciasId.getString("NAME","");
        if(name != ""){
            usuarioCiD.setText(name);
        }

        atras = (Button)findViewById(R.id.btn_atras);
        alertaD = (TextView)findViewById(R.id.tv_alertaId);//Resibe el alertaId de la notificacion


        atras.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(CrearV.this, Visualizacion.class));
            }
        });

        /*ServicioPeticion service = Api.getApi(CrearV.this).create(ServicioPeticion.class);
        Call<Crear_Vista> CrearAlertaCall = service.getCrearVista(usuarioCiD.getText().toString(), alertaD.getText().toString());
        CrearAlertaCall.enqueue(new Callback<Crear_Vista>() {
            @Override
            public void onResponse(Call<Crear_Vista> call, Response<Crear_Vista> response) {
                Crear_Vista peticion = response.body();

                if (response.body() == null){
                    Toast.makeText(CrearV.this,"Ocurrio un error, intentalo mas tarde", Toast.LENGTH_LONG).show();
                    return;
                }
                if(peticion.estado == "true"){

                    Toast.makeText(CrearV.this, "Vista registrada", Toast.LENGTH_LONG).show();
                }else{
                    Toast.makeText(CrearV.this, "No se registro la Vista", Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<Crear_Vista> call, Throwable t) {

                Toast.makeText(CrearV.this, "Error", Toast.LENGTH_LONG).show();

            }
        });*/
    }
}