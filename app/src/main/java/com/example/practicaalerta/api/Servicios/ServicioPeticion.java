package com.example.practicaalerta.api.Servicios;

import com.example.practicaalerta.ViewModels.Alertas;
import com.example.practicaalerta.ViewModels.Alertas_Usuario;
import com.example.practicaalerta.ViewModels.Crear_Alerta;
import com.example.practicaalerta.ViewModels.Crear_Vista;
import com.example.practicaalerta.ViewModels.Peticion_Login;
import com.example.practicaalerta.ViewModels.Registro_Usuario;
import com.example.practicaalerta.ViewModels.Vista_Usuario;
import com.example.practicaalerta.ViewModels.Visualizaciones;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {

    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> registarUsuario(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/login")
    Call<Peticion_Login> getLogin(@Field("username") String correo, @Field("password") String contrasenia);

    @FormUrlEncoded
    @POST("api/crearAlerta")
    Call<Crear_Alerta> getCrearAlerta(@Field("usuarioId") String usuarioIdca);

    @FormUrlEncoded
    @POST("api/alertasusuario")
    Call<Alertas_Usuario> getAlertasUsuario(@Field("usuarioId") String usuarioIda);

    @POST("api/alertas")
    Call<Alertas> getAlertas();

    @FormUrlEncoded
    @POST("api/visualizacionesusuario")
    Call<Vista_Usuario> getVistaUsuario(@Field("usuarioId") String usuarioIdv);

    @POST("api/visualizaciones")
    Call<Visualizaciones> getVistas();

    @FormUrlEncoded
    @POST("api/crearvisualizacionalerta")
    Call<Crear_Vista> getCrearVista(@Field("usuarioId") String usuarioIdv, @Field("alertaId") String alertaIdv);


}
